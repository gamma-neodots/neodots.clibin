#!/usr/bin/env sh
set -e
IFS='
	'
[ "${DTACH_SOCKET:-}" ] && {
	echo >&2 "unset DTACH_SOCKET or exit dtach"
	exit 1
}
dir="${XDG_RUNTIME_DIR:=/run/user/${EUID:=$(id -u)}}/dtach"
mkdir -p "$dir"
export DTACH_SOCKET="${dir}/${1:-sock}"
exec dtach -A "$DTACH_SOCKET" "$SHELL"
