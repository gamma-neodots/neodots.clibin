#!/usr/bin/env bash
IFS=$'\n\t'
# assumes $GITREPOPATH is colon-seperated, possibly globbing
# list of directories containing only git repos
while [[ $# -gt 0 ]]; do
	case $1 in
	-w|--wait)
		wfi=1
		;;
	u|pu|pull)
		git_command () {
			git -C "$1" -c color.ui=always pull --recurse-submodules=on-demand 2> /dev/null |
				sed -e "s/\\(Already\\|remote\\)/$cr$b${1##*/}$n/" -e "s/^Updating/$b${1##*/}$n:/"
		} ;;
	* ) # parse as command
		cmd="$1"
		git_command () {
			git -C "$1" -c color.ui=always "$cmd" 2> /dev/null &&
				echo "$b${dir##*/}$n ${g}${b}done$n" ||
				echo "$b${dir##*/}$n ${r}${b}failed$n"
		} ;;
	esac
shift
done
b=$'\e[1m'
r=$'\e[31m'
g=$'\e[32m'
n=$'\e[0m'
cr=$(tput cr)

# add pass store dir
command -v pass > /dev/null 2>&1 &&
	PASSWORD_STORE_DIR="${PASSWORD_STORE_DIR:-"$HOME/.password-store"}"

typeset -a dirs
IFS=':'
shopt -s dotglob
for repodir in $GITREPOPATH; do
	dirs+=("$repodir/"*)
done
IFS=$OIFS
for dir in "${dirs[@]}" $PASSWORD_STORE_DIR; do
	git_command "$dir" &
done
wait 
if [[ $wfi == 1 ]]; then
	read -r -p "Press any key to continue..." -n1 -s
	echo
fi
