#!/usr/bin/env python3

# -*- coding: utf-8 -*-
import re
import sys

from nvr.nvr import main

def except_handler(_etype, _e, _trace):
    pass

sys.backtracelimit = 0
sys.excepthook = except_handler

if __name__ == '__main__':
    sys.exit(main(
        argv=([re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])]
              + ['--remote-tab-silent', '-s', '-c', 'doautocmd BufEnter']
              + sys.argv[1:])
     ))
